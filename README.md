# GreenweezTest

Dans le cadre d'apprendre a recetter un projet au sein de MDS, nous devons prendre un site en référence et effectuer des tests dessus.

Premiere draft des Uses cases : [Lien du Google Sheet](https://docs.google.com/presentation/d/1FkujF4QYdELCENdwzwZgKGL9jRFUsxUFFyS91kdrlzI/edit#slide=id.g10ff2ed1aba_0_516)

Enoncé du projet : [Recettage Projet / Validation client](https://docs.google.com/presentation/d/1FkujF4QYdELCENdwzwZgKGL9jRFUsxUFFyS91kdrlzI/edit#slide=id.g10ff2ed1aba_0_516)

## Choix de la plateforme

Nous avons choisi de travailler sur une plateforme e-commerce nommée [GreenWeez](https://www.greenweez.com/).

GreenWeez c'est une entreprise qui vend des produits bio en ligne.

## Technologies utilisées

- [Node.js](https://nodejs.org/en/)
- Librairies js utilisée:

  - [Nightwatch.js](https://nightwatchjs.org/)

    ### Qu'est-ce que Nightwatch ?

    Il utilise l'API WebDriver de Selenium pour automatiser les tests d'application web.

    Cet outil permet grâce à une syntaxe simple d'écrire des tests de bout en bout en utilisant JavaScript et des sélecteurs CSS, le tout s'exécutant sur un serveur Selenium.

    Contrairement aux BDD et tests unitaires qui s'exécutent dans un environnement isolé et utilisent des mocks, un test de bout en bout essaie d'émuler, aussi proche que possible, la perception qu'un utilisateur a vis-à-vis d'un système. Dans le cas d'une application web, cela implique de lancer un navigateur, charger des pages, exécuter du JavaScript, interagir avec le DOM, etc.

---

## Comment utiliser les scripts ?

Pour éxécuter les scripts :

1. Cloner le repository sur votre machine

```
Dans votre terminal, accéder au dossier où vous souhaitez que le projet soit.

git clone https://gitlab.com/Kyulee/greenweezTest.git

```

2. Installer sur votre machine

```
npm install
```

3. Lancer les scripts

- Pour lancer tous les scripts :

```
npm nightwatch tests

```

- Pour lancer un seul script :
  ex : npm nightwatch tests/connexion.js

```
npm nightwatch tests/{nom de votre script}.js

```

## Auteurs

Les contributeurs sont :

- Lucie Da Santa (UI/UX designer)
- Camille Bois (UI/UX designer)
- Julie Nguyen (Developpeuse web)

## License

Projet open source.

---

# Test effectués

## Test sur la connexion sur le site

| Etape | Action                                                                          | Résultat attendu                       | Résultat |
| ----- | ------------------------------------------------------------------------------- | -------------------------------------- | -------- |
| 1     | L'utilisateur va sur le site Greenweez                                          | Ouverture de la page Greenweez         | OK       |
| 2     | L'utilisateur clique sur mon compte                                             | Ouverture d'un pop up de connexion     | OK       |
| 3     | L'utilisateur rentre une adresse mail non valide sans le @                      | Message d'erreur                       | OK       |
| 4     | L'utilisateur rentre une adresse mail valide avec compte actif sans mdp         | Message d'erreur                       | OK       |
| 5     | L'utilisateur rentre une adresse mail valide avec compte actif et un mdp valide | Connexion au tableau de bord du client | OK       |

## Test sur la recherche d'un produit sur le site

| Etape | Action                                               | Résultat attendu                                                                  | Résultat |
| ----- | ---------------------------------------------------- | --------------------------------------------------------------------------------- | -------- |
| 1     | L'utilisateur va sur le site Greenweez               | Ouverture de la page Greenweez                                                    | OK       |
| 2     | L'utilisateur se rend sur la barre de recherche      | Curseur sur la barre de recherche                                                 | OK       |
| 3     | L'utilisateur écrit un nom de produit : "soupe"      | Le champ est rempli avec le mot "soupe"                                           | OK       |
| 4     | L'utilisateur clique sur le bouton pour "rechercher" | Ouverture de la page de recherche avec le mot "soupe" & affichage de la recherche | OK       |

## Test sur les produits dans un panier

### Scénario d'ajout d'un produit dans le panier

| Etape | Action                                                                        | Résultat attendu                                                             | Résultat |
| ----- | ----------------------------------------------------------------------------- | ---------------------------------------------------------------------------- | -------- |
| 1     | L'utilisateur va sur le site Greenweez                                        | Ouverture de la page Greenweez                                               | OK       |
| 2     | L'utilisateur cherche le mot "soupe" dans la barre de recherche               | Curseur sur la barre de recherche et le champ est rempli avec le mot "soupe" | OK       |
| 3     | L'utilisateur clique sur le premier produit et arriver sur la page du produit | Affichage de la page du premier produit                                      | OK       |
| 4     | L'utilisateur clique sur le bouton "ajouter au panier"                        | Le produit est ajouté au panier                                              | OK       |
| 5     | L'utilisateur vérifie votre panier en cliquant sur le bouton panier           | Vous constatez que votre panier est maintenant de 1 article.                 | OK       |

### Scénario d'ajout d'un produit dans le panier

| Etape | Action                                                              | Résultat attendu                                             | Résultat |
| ----- | ------------------------------------------------------------------- | ------------------------------------------------------------ | -------- |
| 1     | L'utilisateur va sur le site Greenweez                              | Ouverture de la page Greenweez                               | KO       |
| 2     | L'utilisateur va dans la barre de menu et clique sur l'icone panier | COuverture de la pagne "mon panier"                          | KO       |
| 3     | L'utilisateur clique sur le "-" du produit qu'il vient d'ajouter    | Message "votre panier est vide"                              | KO       |
| 4     | L'utilisateur clique sur le bouton "ajouter au panier"              | Le produit est ajouté au panier                              | KO       |
| 5     | L'utilisateur vérifie votre panier en cliquant sur le bouton panier | Vous constatez que votre panier est maintenant de 1 article. | KO       |

### Scénario d'ajout de plusieurs produit dans le panier

| Etape | Action                                                                                 | Résultat attendu                                             | Résultat |
| ----- | -------------------------------------------------------------------------------------- | ------------------------------------------------------------ | -------- |
| 1     | L'utilisateur va sur le site Greenweez                                                 | Ouverture de la page Greenweez                               | KO       |
| 2     | L'utilisateur va dans la barre de menu et clique sur l'icone panier                    | Couverture de la pagne "mon panier"                          | KO       |
| 3     | L'utilisateur clique sur le premier produit et arriver sur la page du produit          | Affichage de la page du premier produit                      | KO       |
| 4     | L'utilisateur clique sur le bouton "ajouter au panier"                                 | Le produit est ajouté au panier                              | KO       |
| 5     | L'utilisateur souhaites acheter deux fois la même soupe, il va donc cliquer sur le "+" | Le produit est ajouté au panier                              | KO       |
| 5     | L'utilisateur vérifie votre panier en cliquant sur le bouton panier                    | Vous constatez que votre panier est maintenant de 2 article. | KO       |

## Test sur l'ajout d'un produit dans ma weezlist (liste de favoris)

| Etape | Action                                               | Résultat attendu                             | Résultat |
| ----- | ---------------------------------------------------- | -------------------------------------------- | -------- |
| 1     | L'utilisateur va sur le site Greenweez               | Ouverture de la page Greenweez               | KO       |
| 2     | L'utilisateur se connecte à son espace client        | Ouverture du pop up de conexion              | KO       |
| 3     | L'utilisateur choisit une catégorie ex : fruits secs | Ouverture de la page catégorie "fruits secs" | KO       |
| 4     | L'utilisateur rechercher : ifusio cbd                | Ouverture page recherche                     | KO       |
| 5     | L'utilisateur choisis le produit tisane au chanvre   | Ouverture page produit                       | KO       |
| 6     | L'utilisateur ajoute le produit dans sa weezlist     | Curseur sur l'icone favori                   | KO       |
| 7     | L'utilisateur rechercher : ifusio cbd                | Curseur sur l'icone weezlist                 | KO       |
| 8     | L'utilisateur vérifie sa weezlist                    | curseur sur l'icone supression               | KO       |

## Test sur l'envoi d'un formulaire de contact

| Etape | Action                                                             | Résultat attendu                                                  | Résultat |
| ----- | ------------------------------------------------------------------ | ----------------------------------------------------------------- | -------- |
| 1     | L'utilisateur va sur le site Greenweez                             | Ouverture de la page Greenweez                                    | KO       |
| 2     | L'utilisateur va sur la barre du menu                              | Ouverture de la page "https://www.greenweez.com/customer-service" | KO       |
| 3     | L'utilisateur clique sur le bouton contact                         | Ouverture de la page contact                                      | KO       |
| 4     | L'utilisateur clique sur le bouton "besoin d'aide" en bas à droite | Ouverture d'un chat                                               | KO       |
| 4     | L'utilisateur se connecte sur le site                              | Connection a un compte greenweez                                  | KO       |
| 4     | L'utilisateur va sur la barre du menu                              | Ouverture de la page de contact                                   | KO       |
| 4     | L'utilisateur clique sur le bouton contact                         | Ouverture de la page contact                                      | KO       |
| 4     | L'utilisateur insérer un email                                     | Insertion de l'email qui est obligatoire                          | KO       |
| 4     | L'utilisateur insérer un objet                                     | Insertion du sujet dans l'objet                                   | KO       |
| 4     | L'utilisateur Insére son commentaire                               | Insertion des commentaires                                        | KO       |
| 4     | L'utilisateur envoie le message en cliquant sur envoyer            | Redirection vers page d'accueil                                   | KO       |
| 4     | L'utilisateur a un message de confirmation                         | Message de confirmation succes : Le message a bien été envoyé !   | KO       |
