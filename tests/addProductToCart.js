module.exports = {
  "step one: navigate to greenweez": function (browser) {
    browser
      .url("https://www.greenweez.com/")
      .waitForElementVisible("body")
      .assert.titleContains("Greenweez");
  },

  "step two: search the word soupe": function (browser) {
    browser.assert
      .visible("input[type=search]")
      .setValue("input[type=search]", "soupe")
      .click("button[id=onetrust-accept-btn-handler]")
      .assert.visible("button[type=submit]")
      .click("button[type=submit]")
      .waitForElementVisible("body", 1000);
  },
  "step three: Go to product page": function (browser) {
    browser.assert
      .visible("a[data-insights-object-id='68867_0_0']")
      .click("a[data-insights-object-id='68867_0_0']");
  },
  "step four: add a product to cart": function (browser) {
    browser
      .click("div[id='add_product_to_basket']")
      .waitForElementVisible("body");
  },
  "step five : check my cart": function (browser) {
    browser.click("a[id='link_monpanier']").pause(1000);
  },
};
