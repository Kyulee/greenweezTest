module.exports = {
  "step one: navigate to greenweez": function (browser) {
    browser
      .url("https://www.greenweez.com/")
      .waitForElementVisible("body")
      .assert.titleContains("Greenweez");
  },

  "step two: search the word soupe": function (browser) {
    browser.assert
      .visible("input[type=search]")
      .setValue("input[type=search]", "soupe")
      .click("button[id=onetrust-accept-btn-handler]")
      .assert.visible("button[type=submit]")
      .click("button[type=submit]")
      .waitForElementVisible("body", 1000)
      .end();
  },
};
