module.exports = {
  "step one: navigate to greenweez": function (browser) {
    browser
      .url("https://www.greenweez.com/")
      .waitForElementVisible("body")
      .assert.titleContains("Greenweez");
  },

  "step two: search the word soupe": function (browser) {
    browser.assert
      .visible("input[type=search]")
      .setValue("input[type=search]", "soupe")
      .click("button[id=onetrust-accept-btn-handler]")
      .assert.visible("button[type=submit]")
      .click("button[type=submit]")
      .waitForElementVisible("body", 1000);
  },
  "step three: add a product to weezlist": function (browser) {
    browser.assert
      .visible("button[data-products-id='68867']")
      .click("button[data-products-id='68867']")
      .waitForElementVisible("body");
  },
  "step four : check my cart": function (browser) {
    browser.click("a[id='link_monpanier']").waitForElementVisible("body", 1000);
  },
  // "step two: connetion to my account ": function (browser) {
  //   browser.assert
  //     .visible("a[id=menu_compte]")
  //     .click("a[id=menu_compte]")
  //     .waitForElementVisible("form")
  //     .setValue(
  //       "input[id='email_address_responsive']",
  //       "camille.boispro@gmail.com"
  //     )
  //     .sendKeys("input[type=password]", "ac4eU9sVuSBAsPA")
  //     .click("button[id='formulaire_connexion_mon_compte_responsive']")
  //     .waitForElementVisible("body", 1000);
  // },
  // "step five: Enter good email and password access to account": function (
  //   browser
  // ) {
  //   browser
  //     .setValue(
  //       "input[id='email_address_responsive']",
  //       "camille.boispro@gmail.com"
  //     )
  //     .sendKeys("input[type=password]", "ac4eU9sVuSBAsPA")
  //     .click("button[id='formulaire_connexion_mon_compte_responsive']")
  //     .waitForElementVisible("body", 1000);
  // },
};
