module.exports = {
  "step one: navigate to greenweez": function (browser) {
    browser
      .url("https://www.greenweez.com/")
      .waitForElementVisible("body")
      .assert.titleContains("Greenweez")
      .click("button[id=onetrust-accept-btn-handler]");
  },

  "step two: click to my account": function (browser) {
    browser.assert
      .visible("a[id=menu_compte]")
      .click("a[id=menu_compte]")
      .waitForElementVisible("form");
  },
  "step three: Enter wrong email": function (browser) {
    browser
      .setValue("input[id='email_address_responsive']", "testtest")
      .sendKeys("input[type=password]", "DJSQJDSOQ325")
      .click("button[id='formulaire_connexion_mon_compte_responsive']")
      .assert.visible("div[id='message_alerte_connexion']")
      .waitForElementVisible("body", 1000);
  },

  "step four: Enter wrong password": function (browser) {
    browser
      .setValue("input[id='email_address_responsive']", "testtest@gmail.com")
      .click("button[id='formulaire_connexion_mon_compte_responsive']")
      .assert.visible("div[id='message_alerte_connexion']")
      .waitForElementVisible("body", 1000);
  },

  "step five: Enter good email and password access to account": function (
    browser
  ) {
    browser
      .setValue(
        "input[id='email_address_responsive']",
        "camille.boispro@gmail.com"
      )
      .sendKeys("input[type=password]", "ac4eU9sVuSBAsPA")
      .click("button[id='formulaire_connexion_mon_compte_responsive']")
      .waitForElementVisible("body", 1000);
  },
};
