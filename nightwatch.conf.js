module.exports = {
  src_folders: "tests",
  output_folder: "reports",

  webdriver: {
    start_process: true,
    server_path: "./node_modules/.bin/chromedriver",
    host: "localhost",
    port: 4444,
  },
  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: "chrome",
      },
    },
  },
};
